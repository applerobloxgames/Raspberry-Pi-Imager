# Raspberry-Pi-Imager
Raspberry Pi Imager is the quick and easy way to install Raspberry Pi OS and other operating systems to a microSD card, ready to use with your Raspberry Pi.  Download and install Raspberry Pi Imager to a computer with an SD card reader. Put the SD card you'll use with your Raspberry Pi into the reader and run Raspberry Pi Imager.

# To Install Raspberry Pi Imager
Compatible operating systems: Raspberry Pi OS and most other Debian-based ARM operating systems like Twister OS, MX Linux, Kali Linux, and Ubuntu.
```
sudo apt install rpi-imager
```

# To Install Raspberry Pi Imager with (MacOS, Windows & Linux)
Linux x86: 
```
https://downloads.raspberrypi.org/imager/imager_1.6.2_amd64.deb
```

Windows 32bit & 64bit: 
```
https://downloads.raspberrypi.org/imager/imager_1.6.2.exe
```

MacOS: 
```
https://downloads.raspberrypi.org/imager/imager_1.6.2.dmg
```

# Screenshot
Coming soon
